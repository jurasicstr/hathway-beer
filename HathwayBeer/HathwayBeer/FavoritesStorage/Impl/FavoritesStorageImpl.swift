//
//  FavoritesStorageImpl.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

final class FavoritesStorageImpl: FavoritesStorage {
    var observer: FavoritesStorageObserver?
    private var favoritesBeers = Set<BeerModel>() {
        didSet {
            observer?(favoritesBeers)
        }
    }

    func add(beer model: BeerModel) {
        favoritesBeers.insert(model)
    }

    func remove(beer model: BeerModel) {
        favoritesBeers.remove(model)
    }

    func contains(beer model: BeerModel) -> Bool {
        return favoritesBeers.contains(model)
    }

    func getAllBeers() -> Set<BeerModel> {
        return favoritesBeers
    }
}
