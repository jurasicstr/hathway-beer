//
//  FavoritesStorage.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

typealias FavoritesStorageObserver = (Set<BeerModel>) -> Void

protocol FavoritesStorage {
    var observer: FavoritesStorageObserver? { get set }

    func add(beer model: BeerModel)
    func remove(beer model: BeerModel)
    func contains(beer model: BeerModel) -> Bool
    func getAllBeers() -> Set<BeerModel>
}
