//
//  UIView+fill.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

extension UIView {
    func fill(with view: UIView) {
        guard self != view else { fatalError("Attempt to insert view inside itself") }
        addSubview(view)

        let views = ["view": view]
        let hConstraintString = "H:|-0-[view]-0-|"
        let vConstraintString = "V:|-0-[view]-0-|"

        let constraints = [hConstraintString, vConstraintString].flatMap {
            NSLayoutConstraint.constraints(withVisualFormat: $0, metrics: nil, views: views)
        }

        NSLayoutConstraint.activate(constraints)
    }
}
