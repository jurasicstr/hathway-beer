//
//  BeerDetailsViewController.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

final class BeerDetailsViewController: UIViewController {

    private struct Constants {
        static let animationDuration: TimeInterval = 0.2
        static let title = "Beer details"
    }

    @IBOutlet private var beerNameLabel: UILabel!
    @IBOutlet private var beerDescriptioLabel: UILabel!
    @IBOutlet private var beerImageView: UIImageView!
    private weak var coordinator: ApplicationCoordinator?
    private var storage: FavoritesStorage!
    private var addToFavoriteItem: UIBarButtonItem {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "not in favorite"), style: .plain, target: self, action: #selector(addFavoriteAction))
    }
    private var removeFromFavoriteItem: UIBarButtonItem {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "favorite"), style: .plain, target: self, action: #selector(removeFavoriteAction))
    }
    private var beerModel: BeerModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppearance()
        setupText()
        beerModel.map(updateUI)
    }

    func updateUI(with model: BeerModel) {
        let updateBlock = { [weak self] in
            self?.beerNameLabel.text = model.name
            self?.beerDescriptioLabel.text = model.description
            self?.beerImageView.image = model.image
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: Constants.animationDuration, animations: updateBlock)
        }
    }

    private func setupAppearance() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                           target: self,
                                                           action: #selector(doneAction))
        if let model = beerModel {
            let isFavorite = storage.contains(beer: model)
            navigationItem.rightBarButtonItem = isFavorite ? removeFromFavoriteItem : addToFavoriteItem
        }
    }

    private func setupText() {
        title = Constants.title
        [beerNameLabel, beerDescriptioLabel].forEach { $0?.text = "" }
    }

    @objc private func doneAction() {
        coordinator?.dissmissBeerDetails()
    }

    @objc private func addFavoriteAction() {
        beerModel.map(storage.add)
        setupAppearance()
    }

    @objc private func removeFavoriteAction() {
        beerModel.map(storage.remove)
        setupAppearance()
    }
}

private extension BeerDetailsViewController {
    convenience init(model: BeerModel?) {
        self.init()
        beerModel = model
    }
}

extension BeerDetailsViewController {
    static func buildModal(with model: BeerModel, storage: FavoritesStorage, coordinator: ApplicationCoordinator) -> UIViewController {
        let details = BeerDetailsViewController(model: model)
        let controller = UINavigationController(rootViewController: details)
        details.coordinator = coordinator
        details.storage = storage

        return controller
    }

    static func buildEmbedded(with model: BeerModel? = nil) -> BeerDetailsViewController {
        return BeerDetailsViewController(model: model)
    }
}
