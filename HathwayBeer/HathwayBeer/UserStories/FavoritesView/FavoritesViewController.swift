//
//  FavoritesViewController.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

final class FavoritesViewController: UIViewController {
    private struct Constants {
        static let noFavoritesText = "You don't have favorites yet."
    }

    @IBOutlet private var tableView: UITableView!
    private weak var coordinator: ApplicationCoordinator?
    private var storage: FavoritesStorage!
    private lazy var dataSourceAdapter = BeersTableDataSourceAdapter(table: tableView,
                                                                     emptyViewText: Constants.noFavoritesText,
                                                                     deleteAction: storage.remove)

    convenience init(storage: FavoritesStorage, coordinator: ApplicationCoordinator) {
        self.init()
        tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: BeersTabBarItemTags.favorite)
        self.storage = storage
        self.coordinator = coordinator
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        loadFovoritesBeers()
        subscribeToFavorites()
    }
}

extension FavoritesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) }
        guard !dataSourceAdapter.beersDataSource.isEmpty else {
            return
        }
        let model = dataSourceAdapter.beersDataSource[indexPath.row]
        coordinator?.showBeerDetails(with: model)
    }
}

extension FavoritesViewController {
    private func setupTableView() {
        tableView.dataSource = dataSourceAdapter
        tableView.delegate = self
    }

    private func loadFovoritesBeers() {
        dataSourceAdapter.beersDataSource = storage.getAllBeers().map { $0 }
    }

    private func subscribeToFavorites() {
        storage.observer = { [weak dataSourceAdapter] models in
            dataSourceAdapter?.beersDataSource = models.map { $0 }
        }
    }
}
