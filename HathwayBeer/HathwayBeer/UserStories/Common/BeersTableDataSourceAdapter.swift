//
//  BeersTableDataSourceAdapter.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

final class BeersTableDataSourceAdapter: NSObject {

    private struct Constants {
        static let beerCellReuseIdentifier = "BeerCell"
    }

    private weak var tableView: UITableView?
    private var emptyViewText: String = ""
    private var deleteAction: ((BeerModel) -> Void)?
    var filter: String = "" {
        didSet {
            tableView?.reloadData()
        }
    }
    var filteredDataSource: BeersModelList {
        guard !filter.isEmpty else {
            return beersDataSource
        }
        let filterLowercased = filter.lowercased()
        return beersDataSource.filter { $0.name.lowercased().contains(filterLowercased) }
    }
    var beersDataSource: BeersModelList = [] {
        didSet {
            DispatchQueue.main.async { [weak tableView] in
                tableView?.reloadData()
            }
        }
    }

    convenience init(table view: UITableView?, emptyViewText: String, deleteAction: ((BeerModel) -> Void)? = nil) {
        self.init()
        tableView = view
        self.emptyViewText = emptyViewText
        self.deleteAction = deleteAction
    }
}

extension BeersTableDataSourceAdapter: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beersDataSource.isEmpty ? 1 : filteredDataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseId = Constants.beerCellReuseIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId) ?? UITableViewCell(style: .subtitle, reuseIdentifier: reuseId)

        guard !filteredDataSource.isEmpty else {
            return applyEmptyState(for: cell)
        }
        cell.configure(with: filteredDataSource[indexPath.row])

        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else {
            return
        }
        deleteAction?(filteredDataSource[indexPath.row])
    }
}

private extension BeersTableDataSourceAdapter {
    func applyEmptyState(for cell: UITableViewCell) -> UITableViewCell {
        cell.configure(with: emptyViewText)
        tableView?.separatorStyle = .none
        return cell
    }
}

private extension UITableViewCell {
    func configure(with title: String) {
        textLabel?.text = title
        detailTextLabel?.text = ""
        imageView?.image = UIImage()
    }

    func configure(with model: BeerModel) {
        textLabel?.text = model.name
        imageView?.image = model.image
    }
}
