//
//  BeersTabBarItemTags.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

struct BeersTabBarItemTags {
    static let beerList = 0
    static let random = 1
    static let favorite = 2
}
