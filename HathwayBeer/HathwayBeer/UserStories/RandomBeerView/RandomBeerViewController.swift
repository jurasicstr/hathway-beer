//
//  SecondViewController.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

final class RandomBeerViewController: UIViewController {

    private struct Constants {
        static let title = "Random"
    }

    @IBOutlet private var containerView: UIView!
    private var beerDetailsView: BeerDetailsViewController?
    private var beersService: BeersService!

    convenience init(beersService: BeersService) {
        self.init()
        tabBarItem = UITabBarItem(title: Constants.title, image: #imageLiteral(resourceName: "shuffle"), tag: BeersTabBarItemTags.random)
        self.beersService = beersService
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addBeerDetails()
    }
}

private extension RandomBeerViewController {
    @IBAction private func getRandomBeerAction(_ sender: Any) {
        beersService.fetchRandomBeer(completion: beerHandler)
    }

    private func addBeerDetails() {
        let controller = BeerDetailsViewController.buildEmbedded()
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(controller)
        containerView.addSubview(controller.view)
        containerView.fill(with: controller.view)
        controller.didMove(toParent: self)
        beerDetailsView = controller
    }

    private func beerHandler(result: Result<BeerModel>) {
        switch result {
        case .failure(let error):
            print("Error: \(error)")
        case .success(let model):
            beerDetailsView?.updateUI(with: model)
        }
    }
}
