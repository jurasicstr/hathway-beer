//
//  ApplicationCoordinatorImpl.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

final class ApplicationCoordinatorImpl: ApplicationCoordinator {
    private let window: UIWindow
    private let rootViewController = DashboardViewController()
    private let beersService: BeersService = NetworkService()
    private let storage: FavoritesStorage = FavoritesStorageImpl()

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let beerList = BeerListViewController(beersService: beersService, coordinator: self)
        let tabs: [UIViewController] = [
            embeddInNavigation(view: beerList),
            RandomBeerViewController(beersService: beersService),
            FavoritesViewController(storage: storage, coordinator: self)
        ]
        rootViewController.setViewControllers(tabs, animated: false)
        window.rootViewController = rootViewController
    }

    func showBeerDetails(with model: BeerModel) {
        let controller = BeerDetailsViewController.buildModal(with: model, storage: storage, coordinator: self)
        rootViewController.present(controller, animated: true)
    }

    func dissmissBeerDetails() {
        rootViewController.dismiss(animated: true)
    }
}

private extension ApplicationCoordinatorImpl {
    private func embeddInNavigation(view controller: UIViewController) -> UIViewController {
        let navigation = UINavigationController(rootViewController: controller)
        return navigation
    }
}
