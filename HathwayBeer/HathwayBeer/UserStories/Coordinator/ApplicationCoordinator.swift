//
//  ApplicationCoordinator.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

protocol ApplicationCoordinator: AnyObject {
    func start()
    func showBeerDetails(with model: BeerModel)
    func dissmissBeerDetails()
}
