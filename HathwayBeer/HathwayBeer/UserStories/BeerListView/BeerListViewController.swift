//
//  FirstViewController.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

final class BeerListViewController: UIViewController {
    private struct Constants {
        static let animationDuration: TimeInterval = 0.2
        static let title = "Beers list"
        static let tabTitle = "Beers"
        static let loadingText = "Loading, please wait..."
        static let searchBarPlaceholder = "Search Beers"
    }

    @IBOutlet private var tableView: UITableView!
    private lazy var dataSourceAdapter = BeersTableDataSourceAdapter(table: tableView, emptyViewText: Constants.loadingText)
    private let footerActivityIndicator = UIActivityIndicatorView(style: .gray)
    private weak var coordinator: ApplicationCoordinator?
    private var beersService: BeersService!
    private var loadedPagesNumber: UInt = 0
    private var isAllBeersFetched = false
    private var isLoading: Bool = false {
        didSet {
            DispatchQueue.main.async { [weak spinner = self.footerActivityIndicator, isLoading] in
                isLoading ? spinner?.startAnimating() : spinner?.stopAnimating()
            }
        }
    }
    private let searchController = UISearchController(searchResultsController: nil)

    convenience init(beersService: BeersService, coordinator: ApplicationCoordinator) {
        self.init()
        tabBarItem = UITabBarItem(title: Constants.tabTitle, image: #imageLiteral(resourceName: "beer"), tag: BeersTabBarItemTags.beerList)
        self.beersService = beersService
        self.coordinator = coordinator
    }

    override func viewDidLoad() {
        assert(beersService != nil, "BeersService needs to be injected!")
        assert(coordinator != nil, "ApplicationCoordinator needs to be injected!")

        super.viewDidLoad()
        setupTableView()
        setupSearch()
        requestNewData()
        title = Constants.title
    }
}

extension BeerListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) }
        guard !dataSourceAdapter.filteredDataSource.isEmpty else {
            return
        }
        let model = dataSourceAdapter.filteredDataSource[indexPath.row]
        coordinator?.showBeerDetails(with: model)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard !isLoading && !isAllBeersFetched && tableView.isLastRow(for: indexPath) else {
            return
        }
        addFooter(with: cell.bounds.size)
        requestNewData()
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
}

extension BeerListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        dataSourceAdapter.filter = searchController.searchBar.text ?? ""
    }
}

private extension BeerListViewController {
    private func handleBeers(result: Result<BeersModelList>) {
        switch result {
        case .success(let beers):
            handleNew(beers: beers)
        case .failure(let error):
            print("Error: \(error)")
            isLoading = false
        }
    }

    private func handleNew(beers: BeersModelList) {
        if beers.isEmpty {
            allDataFetched()
        } else {
            loadedPagesNumber += 1
        }
        applyNonEmptyStyle()
        dataSourceAdapter.beersDataSource.append(contentsOf: beers)
        isLoading = false
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = dataSourceAdapter
        tableView.keyboardDismissMode = .onDrag
    }

    private func setupSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = Constants.searchBarPlaceholder
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    private func requestNewData() {
        guard !isLoading else {
            return
        }
        isLoading = true
        beersService.fetchBeers(page: loadedPagesNumber + 1, completion: handleBeers)
    }

    private func addFooter(with size: CGSize) {
        footerActivityIndicator.frame = CGRect(origin: .zero, size: size)
        tableView.tableFooterView = footerActivityIndicator
        tableView.tableFooterView?.isHidden = false
    }

    private func applyNonEmptyStyle() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.separatorStyle = .singleLine
        }
    }

    private func allDataFetched() {
        isAllBeersFetched = true
        isLoading = false
        DispatchQueue.main.async { [weak self] in
            self?.tableView.tableFooterView?.isHidden = false
        }
    }
}

private extension UITableView {
    func isLastRow(for indexPath: IndexPath) -> Bool {
        let lastSection = numberOfSections - 1
        let lastRow = numberOfRows(inSection: lastSection) - 1

        return lastSection == indexPath.section && lastRow == indexPath.row
    }
}
