//
//  BeerModel.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

typealias BeersDtoList = [BeerDataDto]

/// DTO is Data transfer object from json
struct BeerDataDto: Codable {
    let name: String
    let imageUrl: URL?
    let description: String
}

extension BeerDataDto: CustomDebugStringConvertible {
    public var debugDescription: String {
        return "\n name: \(name) image url: \(String(describing: imageUrl?.absoluteString))"
    }
}
