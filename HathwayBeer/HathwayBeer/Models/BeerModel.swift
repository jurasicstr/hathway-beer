//
//  BeerModel.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import UIKit

typealias BeersModelList = [BeerModel]

struct BeerModel: Hashable {
    let name: String
    let imageData: Data
    let description: String
}

extension BeerModel {
    var image: UIImage {
        return UIImage(data: imageData) ?? #imageLiteral(resourceName: "no image")
    }
}
