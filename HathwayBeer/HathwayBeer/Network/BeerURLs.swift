//
//  BeerURLs.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

struct BeerURLs {
    private static let base = "https://api.punkapi.com/v2"
    static let beerList = URL(string: base + "/beers")!
    static let randomBeer = URL(string: base + "/beers/random")!
}
