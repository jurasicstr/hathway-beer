//
//  NetworkService.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

final class NetworkService: BeersService {

    private struct Constants {
        static let httpHeaderJsonValue = "application/json"
        static let httpHeaderContentField = "Content-Type"
        static let httpHeaderAcceptField = "Accept"
        static let httpGetMethod = "GET"
        static let pageIndexQueryName = "page"
        static let defaultPageLimit: UInt = 25
        static let pageLimite = URLQueryItem(name: "per_page", value: "\(defaultPageLimit)")
    }

    private lazy var jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    func fetchBeers(page index: UInt, completion handler: @escaping BeersResultHandler) {
        guard let url = getBeersPageUrl(for: index) else {
            handler(.failure(NetworkError.invalidRequest))
            return
        }

        let beersHandler: (Result<BeersDtoList>) -> Void = { [weak self] in
            switch $0 {
            case .failure(let error):
                handler(.failure(error))
            case .success(let list):
                self?.fetchAllImages(from: list, then: handler)
            }
        }

        getDtoModel(from: url, completion: beersHandler)
    }

    func fetchRandomBeer(completion handler: @escaping SingleBeerResultHandler) {
        let beersHandler: (Result<BeersDtoList>) -> Void = { [weak self] in
            switch $0 {
            case .failure(let error):
                handler(.failure(error))
            case .success(let list):
                guard let beer = list.first else {
                    handler(.failure(NetworkError.invalidResponse))
                    return
                }
                self?.fetchImage(for: beer, then: handler)
            }
        }

        getDtoModel(from: BeerURLs.randomBeer, completion: beersHandler)
    }

}

private extension NetworkService {
    private func getBeersPageUrl(for index: UInt) -> URL? {
        let pageIndexQuery = URLQueryItem(name: Constants.pageIndexQueryName, value: "\(index)")
        var components = URLComponents(url: BeerURLs.beerList, resolvingAgainstBaseURL: false)
        components?.queryItems = [pageIndexQuery, Constants.pageLimite]

        return components?.url
    }

    private func fetchImage(for model: BeerDataDto, then handler: @escaping SingleBeerResultHandler) {
        fetchAllImages(from: [model]) { result in
            switch result {
            case .failure(let error):
                    handler(.failure(error))
            case .success(let list):
                guard let beer = list.first else {
                    handler(.failure(NetworkError.invalidResponse))
                    return
                }
                handler(.success(beer))
            }
        }
    }

    private func fetchAllImages(from list: BeersDtoList, then handler: @escaping BeersResultHandler) {
        guard !list.isEmpty else {
            handler(.success(BeersModelList()))
            return
        }

        let urls: [URL] = list.compactMap { $0.imageUrl }
        downloadAll(from: urls) { [weak self] images in
            self?.composeBeersModelList(from: list, images: images, then: handler)
        }
    }

    private func downloadAll(from urls: [URL], then handler: @escaping ([URL: Data]) -> Void) {
        guard !urls.isEmpty else {
            handler([:])
            return
        }

        var requestCounter = urls.count
        var downloadedImages = [URL: Data]()
        let mutex = NSRecursiveLock()

        urls.forEach { url in
            getRawData(from: url) { [weak self] (data, response, error) in
                mutex.lock()
                defer { mutex.unlock() }

                if let data = data, self?.validateResponse(response: response, error: error) == nil {
                    downloadedImages[url] = data
                }
                requestCounter -= 1
                if requestCounter == 0 {
                    handler(downloadedImages)
                }
            }
        }
    }

    private func composeBeersModelList(from dto: BeersDtoList, images: [URL: Data], then handler: @escaping BeersResultHandler) {
        let beers: BeersModelList = dto.map { model in
            var imageData = Data()
            model.imageUrl.map { imageData = images[$0] ?? Data() }
            return BeerModel(name: model.name, imageData: imageData, description: model.description)
        }

        handler(.success(beers))
    }

    private func getRawData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

    private func getDtoModel<T: Decodable>(from url: URL, completion handler: @escaping (Result<T>) -> Void) {
        let request = createGetRequest(from: url)
        let task = URLSession.shared.dataTask(with: request) { [weak self, jsonDecoder] data, response, error in
            if let error = self?.validateResponse(response: response, error: error) {
                handler(.failure(error))
                return
            }

            guard let data = data else {
                handler(.failure(NetworkError.dataIsEmpty))
                return
            }

            do {
                let model = try jsonDecoder.decode(T.self, from: data)
                handler(.success(model))
            } catch let error {
                print("Error: \(error)")
                handler(.failure(NetworkError.invalidDataInResponse))
            }

        }
        task.resume()
    }

    private func validateResponse(response: URLResponse?, error: Error?) -> Error? {
        if let error = error {
            return error
        }

        guard response?.isValid == true else {
            return NetworkError.invalidResponse
        }

        return nil
    }

    private func createGetRequest(from url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = Constants.httpGetMethod
        [Constants.httpHeaderContentField, Constants.httpHeaderAcceptField].forEach {
            request.addValue(Constants.httpHeaderJsonValue, forHTTPHeaderField: $0)
        }

        return request
    }
}

private extension URLResponse {
    private static let validHttpCodes = 200...299

    var isValid: Bool {
        guard let httpResponse = self as? HTTPURLResponse else {
            return false
        }

        return URLResponse.validHttpCodes.contains(httpResponse.statusCode)
    }
}
