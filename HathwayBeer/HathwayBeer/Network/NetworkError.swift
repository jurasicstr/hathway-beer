//
//  NetworkError.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case dataIsEmpty
    case invalidRequest
    case invalidResponse
    case invalidDataInResponse
}
