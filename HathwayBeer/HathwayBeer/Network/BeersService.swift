//
//  BeersService.swift
//  HathwayBeer
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import Foundation

typealias BeersResultHandler = (Result<BeersModelList>) -> Void
typealias SingleBeerResultHandler = (Result<BeerModel>) -> Void

protocol BeersService: AnyObject {
    func fetchBeers(page index: UInt, completion handler: @escaping BeersResultHandler)
    func fetchRandomBeer(completion handler: @escaping SingleBeerResultHandler)
}
