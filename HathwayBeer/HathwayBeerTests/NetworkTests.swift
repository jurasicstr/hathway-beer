//
//  NetworkTests.swift
//  HathwayBeerTests
//
//  Created by Iurii on 3/29/19.
//  Copyright © 2019 Iurii. All rights reserved.
//

import XCTest
@testable import HathwayBeer

final class NetworkTests: XCTestCase {
    private let expectationTimeOut: Double = 5
    private let networkService = NetworkService()

    func testFetchBeersModelList() {
        let expectation = self.expectation(description: #function)

        networkService.fetchBeers(page: 1) { result in
            switch result {
            case .success(let beerList):
                print("Beers list: \(beerList)")
                expectation.fulfill()
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        waitForExpectations(timeout: expectationTimeOut, handler: nil)
    }

    func testFetchRandomBeerModel() {
        let expectation = self.expectation(description: #function)

        networkService.fetchRandomBeer { result in
            switch result {
            case .success(let beer):
                print("Random beer: \(beer)")
                expectation.fulfill()
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        waitForExpectations(timeout: expectationTimeOut, handler: nil)
    }

}
